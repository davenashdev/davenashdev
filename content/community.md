---
title: Community
subtitle: Communities & Organizations
date: 20201-01-02T19:07:23-08:00
comments: false
---

[DevOpsDays Seattle 20201](https://devopsdays.org/events/2021-seattle/welcome/)

[CoffeeOps](http://www.coffeeops.org/)

[Seattle DevOps Meetup](https://www.seattledevops.net/)

[West Seattle Linux Users Group](https://wsealug.net/)

[Seattle Matrix](https://seattlematrix.org/)

