---
title: "Episode 20210101"
date: 2021-01-01T19:47:28-08:00
author: Dave Nash
draft: false
tags:
    - 51Projects
    - golang
    - twitch
    - youtube
    - tic-tac-toe
---
## tic-tac-toe Part 1

Well Major Mess up I forgot to add Audio to the coding scene in OBS so unless you are an expert lip reader it is all silent! Also, lost the stream/recording after a few minutes so it is broken into 2 parts.

At least the improvements for next time are easy to identify.

### tic-tac-toe Part 1A
{{< youtube g1QDPcmBwK0 >}}

### tic-tac-toe Part 1B
{{< youtube y3Q7IHhAmFI >}}